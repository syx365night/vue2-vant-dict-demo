# vue2-vant-dict

​		vue2-vant-dict是在vue2框架下对vant组件库的部分组件进行二开，实现更轻松使用字典数据的字典包插件。引入此包后，可轻松实现select下拉选项，radio单选框，checkbox多选框，cascader联级选项，tree树形控件，multiple-select多选组件，脱敏表单等组件。拥有多种字典相关方法及日期格式化，脱敏等方法。

**示例：实现cascader**

```vue
<van-cascader-dict :field-names="{text: 'areaName', value: 'id', children: 'children'}" @dictChange="handleChange" label="地区(基础用法)" label-width="110px" title="地区选择" input-align="right"  dictType="city" placeholder="请选择地区" v-model="value1" ></van-cascader-dict>
```

**效果如下**

![el-select-dict展示效果](http://qiniu.xiaobusoft.com/npm/van-cascader-dict.gif)

**示例2：实现select**

```vue
<van-select-dict @dictChange="handleChange" label="性别(基础用法)" label-width="100px" input-align="right" dictType="AAC004" placeholder="请选择性别" v-model="value1" ></van-select-dict>
```

![el-select-dict组件效果](http://qiniu.xiaobusoft.com/npm/van-select-dict.gif)

**示例3：实现输入框非编辑时脱敏**

```vue
<template>
  <div>
    <van-field-dict label="姓名" v-model="value1"></van-field-dict>
    <van-field-dict label="证件号码" v-model="value2"></van-field-dict>
    <van-field-dict label="手机号" v-model="value3"></van-field-dict>
    <van-field-dict label="地址" type="textarea" v-model="value4"></van-field-dict>

    <van-field-dict label="姓名" maskType="name" v-model="value1"></van-field-dict>
    <van-field-dict label="证件号码" maskType="idCard" v-model="value2"></van-field-dict>
    <van-field-dict label="手机号" maskType="mobile" v-model="value3"></van-field-dict>
    <van-field-dict label="地址" type="textarea" maskType="address" v-model="value4"></van-field-dict>
    <van-field-dict label="前三后四" :maskStart="3" :maskEnd="4" v-model="value5"></van-field-dict>
    <van-field-dict label="前三中三后四" :maskStart="3" :maskMiddle="3" :maskEnd="4" v-model="value6"></van-field-dict>
    <van-field-dict label="默认值" defaultVal="恭喜发财" maskType="name" v-model="value7"></van-field-dict>
  </div>
</template>

<script>
export default {
  data() {
    return {
      value1: "沈小布",
      value2: "35062418892031",
      value3: "18859661234",
      value4: "宁夏回族自治区闽宁镇涌泉村芗城路142号",
      value5: "1234567890",
      value6: "13145021314",
      value7: ""
    }
  }
}
</script>

<style>

</style>
```



![el-field-dict组件效果](http://qiniu.xiaobusoft.com/npm/van-field-dict.gif)

一行代码轻松使用，并且解决选项被更换后关闭弹出层后，选项不会复位的问题。

**此处主要讲包的配置及准备工作，具体用法可前往[vue2-vant-dict官网](http://xiaobusoft.com/vue2-vant-dict)查看使用文档，[备用官网](https://shenxiaobu.github.io/vue2-vant-dict/),如有问题可前往[问题反馈表格进行记录](https://docs.qq.com/sheet/DVmZQb0hyTk9uc1dY)， 也可关注微信公众号【爆米花小布】私信进行反馈**

## 使用条件

1. vue2中 2.6以上版本
2. 引入vant相关组件及样式，**由于vant关于vue2版本已经不再更新，但是存在cascader组件禁用无法使用的情况，因此对于需要cascader禁用选项可用的，需将vant组件库更换为 [vant-xiaobu](https://www.npmjs.com/package/vant-xiaobu) 组件库，仅对cascader优化，可放心使用。**
3. 一个请求字典的接口
   1. 不传参可获取全部字典数据
   2. 返回字典包的版本号（字典数据更改时，更改字典包版本号，用于清除在浏览器上缓存的旧的字典包数据）

## 快速开始

以下全部已js为示例，且以最完美的形式配合vue2-vant-dict包的使用。

#### 后端接口要求

1. 获取全局配置接口，配置中包含当前字典版本号。对字典数据进行操作时，需修改字典版本号。
2. 获取字典数据接口，能接受不传参返回所有字典数据，可接受同时获取多个字典类型数据。并且将当前字典版本号返回，用于判断当前项目使用的字典数据是否是数据库最新版的字典数据。

#### 安装包

```shell
npm install vue2-vant-dict
```

#### 新建配置文件

src目录下新建dict-config.js文件，用于配置字典包获取字典的接口，当前字典版本号，数据字典缓存位置等。以下代码列全一点吧, 真实只有 getDictCodeApi 及 version必传

```ts
//引入请求字典接口
import {getDictCodeApi} from "@/api/common-api"
//引入本地缓存数据
import localDictCodes from "@/assets/data/dict"

export default {
  getDictCodeApi: getDictCodeApi, // 获取字典的接口  必传
  version: "0.0.1", // 当前字典版本号 必传 获取全局配置化需 覆盖此版本号数据
  localDictCodes, // 本地字典数据
  versionKey: "xiaobuDictVersion", // 在浏览器缓存的版本号键名 选传 默认值vue3ElementDictVersion
  dictDataKey: "xiaobuDictData", // 在浏览器缓存的字典数据键名 选传 默认值 vue2VantDictData
  query: "type", // 请求字典接口的参数名 选传 默认 dictType
  format: {
  	label: "dictLabel", // 配置字典值和显示
    value: "dictValue", // 配置字典值和显示
    type: "type", // 配置字典类型 部分组件可用
    color: "color", // 配置字典颜色 部分组件可用
    disabled: "disabled" // 配置字典禁用字段名
  }, 
  formatterDictList: (data, query) => {
  	return data.xiaobuDictData
  }, // data为请求字典接口后返回的data数据 格式化返回字典数据，此处return的为字典数据，用于兼容返回的数据结构与默认不一致问题， 选传 默认返回  data.dictData query为请求的字典类型的参数，用于部分接口不按要求，没返回 dictType: [{},{}] 数据结构的形式，此时可利用query自行拼凑成符合的数据格式
  formatterDictVersion: (data) => {
    return data.xiaobuDictVersion
  }, // data为请求字典接口后返回的data数据 格式化返回版本号，用于兼容返回的数据结构与默认不一致问题 默认返回 data.dictVersion 获取到版本号后会 与字典包配置的版本号进行比对
  filterDataFun: (list) => {
      return list.filter(item => item.status === '1')
  }, // 可对返回的字典数据进行过滤 list为某字典类型的数据 选传，默认不过滤  return list
  disabledDataFun: (list) => {
      return list.map(item => {
          ...item,
          disabled: item.isDisabled === '1'
      })
  }, // 可对返回的字典数据配置禁用字段 list为某字典类型的数据 选传，默认不过滤  return list
  formatterRequest: (query, dictType) => {
    // ...此处无法举例 以包默认形式展示
    if (!dictType) {
        return { [query]: "" };
    }
    return { [query]: dictType };
  }, // 格式化请求体，用于兼容接口需要的参数格式，默认dictType为空时 获取全部字典数据，接口需要获取多种字典数据时不同后端开发人员可能需要的格式不一样，因此此处可配置成后端开发人员需要的格式。
    
  storage: localstorage, //数据缓存的位置 默认 localstorage 可选为 sessionstorage 兼容iframe嵌套项目
  treeSetting: {
      labelField: "areaName" //label字段名 默认值id
      parentIdField: "parentId", //父节点唯一标识字段名 默认值 parentId
      childrenField: "children", //子节点标识字段名 默认值 children
      firstId: "0", // 根节点值 默认值 字符串 0
      labelField: "label", //label字段名 默认值 label
      labelArrField: "labelArr", //给对象新增的中文数组字段名 默认值 labelArr 字典包会将数组转化为树形结构数据 并每一项生成 labelArr字段，内容为【爷爷级/父级label,...,自己的label】
      idArrField: "idArr", //给对象新增的id数组字段名 默认值 idArr 字典包会将数组转化为树形结构数据 并每一项生成 labelArr字段，内容为【爷爷级/父级id,...,自己的id】
      levelField: "level", //给对象新增的层级字段名 值为层级
      level: 0, // 给根目录配置的层级 配置根目录层级为 0 级
      leafField: "leaf" //叶子节点标识字段名 值为 true或 false
  } // 对树形数据的配置 选传 字典支持将数组结构转化为树形结构
}
```

### 配置

src下新建文件dict-config.ts，用于配置字典包相关配置。配置项如下

|         字段         |   类型   | 必传 | 说明                                                         |                            默认值                            |
| :------------------: | :------: | :--: | :----------------------------------------------------------- | :----------------------------------------------------------: |
|    getDictCodeApi    | Promise  |  是  | 请求字典接口                                                 |                              —                               |
|       version        |  String  |  是  | 当前字典包版本号 必传                                        |                              —                               |
|    localDictCodes    |  Object  |  否  | 本地字典数据                                                 |                              {}                              |
|      versionKey      |  String  |  否  | 在浏览器缓存的版本号键名                                     |                     vue2VantDictVersion                      |
|     dictDataKey      |  String  |  否  | 在浏览器缓存的字典数据键名                                   |                       vue2VantDictData                       |
|        query         |  String  |  否  | 请求字典接口的参数名                                         |                           dictType                           |
|        format        |  Object  |  否  | 配置字典值和显示 字段的配置项  需同时配置 不可只配一个       |               {label: "label", value: "value"}               |
|  formatterDictList   | Function |  否  | data为请求字典接口后返回的data数据 格式化返回字典数据，此处return的为字典数据，用于兼容返回的数据结构与默认不一致问题， 选传 默认返回  data.dictData query为请求的字典类型的参数，用于部分接口不按要求，没返回 dictType: [{},{}] 数据结构的形式，此时可利用query自行拼凑成符合的数据格式 |               (data) => {return data.dictData}               |
| formatterDictVersion | Function |  否  | data为请求字典接口后返回的data数据 格式化返回版本号，用于兼容返回的数据结构与默认不一致问题 默认返回 data.dictVersion 获取到版本号后会 与字典包配置的版本号进行比对 |               (data) => {return data.version}                |
|    filterDataFun     | Function |  否  | 可对返回的字典数据进行过滤 list为某字典类型的数据            |                   （list) => {return list}                   |
|   disabledDataFun    | Function |  否  | 可对返回的字典数据配置禁用字段 list为某字典类型的数据        |                   (list) => {return list}                    |
|   formatterRequest   | Function |  否  | 兼格式化请求体，用于兼容接口需要的参数格式，默认dictType为空时 获取全部字典数据，接口需要获取多种字典数据时不同后端开发人员可能需要的格式不一样，因此此处可配置成后端开发人员需要的格式 | (query, dictType) => {if(!dictType){return { [query]: "" }} return { [query]: dictType }} |
|       storage        | storage  |  否  | 数据缓存的位置 默认 localstorage 可选为 sessionstorage 兼容  |                         localstorage                         |
|     treeSetting      |  Object  |  否  | 树形相关数据配置                                             |                         继续阅读文档                         |

**localDictCodes的格式及字典数据的的格式如下**

```ts
export default {
  SEX: [
    {
      value: "1",
      label: "男"
    },
    {
      value: "2",
      label: "女"
    },
    {
      value: "3",
      label: "未知"
    }
  ],
  niceOrBad: [
    {
      "value": "0",
      "label": "好"
    }, {
      "value": "1",
      "label": "差"
    }
  ],
  area: [
    {
      "id": "110000",
      "parentId": "0",
      "label": "北京"
    },
    {
      "id": "110100",
      "parentId": "0",
      "label": "北京市"
    },
    {
      "id": "110101",
      "parentId": "110100",
      "areaName": "东城区"
    },
    {
      "id": "110102",
      "parentId": "110100",
      "areaName": "西城区"
    },
    {
      "id": "110105",
      "parentId": "110100",
      "areaName": "朝阳区"
    }
  ]
}
```

:::warning

localDictCodes本地配置的value及label 以及 树形结构的 label及id须与dict-setting中的一致

接口返回的字段的数据也同理

:::

#### treeSetting配置

|     字段      |      类型      | 必传 | 说明                                                         |  默认值  |
| :-----------: | :------------: | :--: | ------------------------------------------------------------ | :------: |
|    idField    |     String     |  否  | 树形数据值的键名                                             |    id    |
| parentIdField |     String     |  否  | 父节点的键名                                                 | parentId |
| childrenField |     String     |  否  | 生成的树形结构子节点的字段名                                 | children |
|    firstId    | String，Number |  否  | 根节点的值                                                   |   “0”    |
|  labelField   |     String     |  否  | 显示的值的字段名                                             |  label   |
| labelArrField |     String     |  否  | 字典包会将数组转化为树形结构数据 并每一项生成 labelArr字段，内容为【爷爷级/父级label,...,自己的label】 | labelArr |
|  idArrField   |     String     |  否  | 字典包会将数组转化为树形结构数据 并每一项生成 labelArr字段，内容为【爷爷级/父级id,...,自己的id】 |  idArr   |
|  levelField   |     String     |  否  | 给对象新增的层级字段名                                       |  level   |
|     level     |     Number     |  否  | 配置根目录的层级                                             |    0     |
|   leafField   |     String     |  否  | 是否叶子节点的字段名 值为boolean                             |   leaf   |

#### 在main.js中的使用

```ts
import Vue from "vue"
import App from "./App.vue"
// 引入组件库
import Vant from "vant-xiaobu"
import "vant-xiaobu/lib/index.css"
Vue.use(Vant)

import router from "@/router"
import { beforeEachHandler } from "@/router/before-each"
import afterEachHandler from "@/router/after-each"
router.beforeEach(beforeEachHandler)
router.afterEach(afterEachHandler)

import {getGlobalConfigApi} from "@/api/module/common-api.js"
import dictConfig from "@/dict-config.js"
import vue2VantDict from "vue2-vant-dict"

//获取当前版本号
getGlobalConfigApi().then(data => {
  const {version} = data
  Object.assign(dictConfig, {version})
  Vue.use(vue2VantDict, dictConfig)
  new Vue({
    router,
    render: h => h(App)
  }).$mount("#app")
})

```

至此已全部配置完成，就可以轻松使用了。



### 使用

此包拥有组件及方法在此列出来

> 组件
>
> > van-select-dict   选项组件
> >
> > van-multiple-select-dict   多选组件
> >
> > van-tabs-dict     tab栏组件
> >
> > van-field-dict     输入框组件
> >
> > van-calendar-dict   日历选择组件
> >
> > van-datetime-dict    日期选择组件
> >
> > van-dropdown-item-dict   配合van-dropdown-menu的下拉列表组件
> >
> > van-radio-dict    单选框组件
> >
> > van-checkbox-dict  复选框组件
> >
> > van-cascader-dict      联级选项组件
> >
> > van-button--dict     按钮组件
> >
> > van-tag-dict     标签组件
> >
>
> 过滤器：字典过滤方法   函数形式调用   也可使用过滤器方式
>
> > getLabelByCodeFilter     
> >
> > getLabelByCodesFilter   
> >
> > getCodeByLabelFilter     
> >
> > getCodeByLabelsFilter   
> >
> > getTreeLabelByCodeFilter    
> >
> > getTreeLabelByCodesFilter  
> >
> > getTreeCodeByLabelFilter   
> >
> > getTreeCodeByLabelsFilter
> >
> > formatDate
> >
> > desensitization
>
> 方法
>
> > 普通方法  函数
> >
> > > getDictConfig
> > >
> > > getDictConfigByKey
> >
> > 字典相关  promise方法
> >
> > > getLabelByCode
> > >
> > > getLabelByCodes
> > >
> > > getCodeByLabel
> > >
> > > getCodeByLabels
> > >
> > > getTreeLabelByCode
> > >
> > > getTreeLabelByCodes
> > >
> > > getTreeCodeByLabel
> > >
> > > getTreeCodeByLabels
> >
> > 日期格式化 函数
> >
> > > formatDate
> > >
> > > isDate
> >
> > 脱敏相关  函数
> >
> > > mask
> > >
> > > maskAddress
> > >
> > > maskIdCard
> > >
> > > maskName
> > >
> > > maskPhone
> > >
> > > desensitization
> >
> > 树形结构相关 函数
> >
> > > listToTree
> > >
> > > getTreeItemByCode
> > >
> > > getTreeItemByLabel



**此处主要讲包的配置及准备工作，具体用法可前往[vue2-element-dict官网](http://xiaobusoft.com/vue2-element-dict)查看使用文档，[备用官网](https://shenxiaobu.github.io/vue2-element-dict/),如有问题可前往[问题反馈表格进行记录](https://docs.qq.com/sheet/DVmZQb0hyTk9uc1dY)， 也可关注微信公众号【爆米花小布】私信进行反馈**

![公众号二维码](http://qiniu.xiaobusoft.com/npm/gongzhonghao_wechat.png)

## 微信赞助

开发不易，如果对您有所帮助，可赞助作者，利于官网服务器运营。您的支持，是我继续努力的最大动力。

![赞助码](http://qiniu.xiaobusoft.com/npm/qr-card.jpg)


## 更新日志

### 2.0.2

1. 【优化】各个字典组件兼容配置 disabled 和 readonly  为赋值时的写法 即 <van-select-dict disabled /> 即可实现disabled
2. 【修复】van-field-dict当为只读且isMask为true，未进行脱敏的问题

### 2.0.1

1. 【优化】van-field-dict组件新增isMask属性配置，用于快速切换脱敏与不脱敏状态

### 2.0.0

1. 【修复】修复van-tabs-dict组件change事件报错bug
2. 【优化】树形方法及过滤器多级时默认分隔符修改为/
3. 【修复】van-radio-dict组件配置keyValue时，返回值修改为对象类型
4. 【修复】修复van-field-dict组件当一开始即为只读模式时，不会展示全部信息，及手机号脱敏默认展示值不会展示的bug
5. 【优化】van-cascader-dict组件新增两个插槽 cascaderOptionsTop cascaderOptionsBottom

### 1.0.9

1. 【优化】select、multiple-select、radio、checkbox四个组件优化自定义列表数据无法控制禁用选项的需求

### 1.0.8

1. 【优化】所有基于van-field的字典组件，插槽均为van-field的，内部插槽仅需 组件名+原型插槽名即可  如van-multiple-select-dict组件即可插入
      <template #cellValue>
        cellValue
      </template>
      <template #cellLabel>
        cellLabel
      </template>
      <template #cellExtra>
        cellExtra
      </template>

2. 【修复】手机号脱敏优化，当手机号不符合规范 仅几位数时 脱敏后原数据失真的bug
3. 【优化】van-radio-dict和vab-checkbox-dict组件，修改为嵌套在van-field组件下，且新增自定义数据 data

### 1.0.6

1. 【优化】format可配置部分字段

2. 【优化】van-button-dict,van-tag-dict组件 调整优先级 disabled属性字典数据优先级最高 color及type属性字典数据优先级最低

   

### 1.0.5
1. 【优化】所有组件均可配置disabled字段名
2. 【优化】van-button-dict,van-tag-dict组件 新增配置color和disabled配置 接受字典数据
3. 【优化】van-field-dict组件只读模式时，不脱敏
4. 【新增】新增van-tabs-dict组件 

### 1.0.3
1. 【优化】formatDate方法兼容 时间格式为世界标准格式 的数据 

### 1.0.2
1. 【新增功能】新增van-multiple-select-dict组件，实现字典多选功能，可配置形状 shape 选中颜色checked-color  自定义图形插槽 形状大小icon-size 最大选择数量 max 等配置 

### 1.0.1
1. 【优化】van-cascader-dict、van-select-dict、van-calendar-dict、van-datetime-dict组件被van-form嵌套时，继承van-form的readonly和disabled属性，去除点击反馈效果

2. 【优化】解决van-cascader-dict、van-select-dict、van-calendar-dict、van-datetime-dict组件被van-form嵌套时底部border不会展示的问题

### 1.0.0
1. 【优化】van-cascader-dict、van-select-dict、van-calendar-dict、van-datetime-dict组件被van-form嵌套时，继承van-form的readonly和disabled属性，实现点击无效的

### 0.0.9
1. 【修复】修复版本英文单词书写错误，导致请求字典时本地字典也会触发请求的问题

### 0.0.8
1. 【优化】cascader组件新增showValueFun属性配置，实现自定义展示页面显示结果 接收两个参数   targetArr, spacer

### 0.0.6
1. 【修复】修复datetime、cascader、calendar组件手动赋值时无法正常更新的问题
2. 【优化】优化部分组件dictChange改变事件时，v-model的值还不是最新值的问题

### 0.0.5
1. 【修复】修复datetime组件手动赋值时无法正常更新的问题

### 0.0.3
1. 【修复】修复cascader字典组件的bug：同个页面同时使用该组件但是 filedNames配置项不一致时出错的问题 优化idfiled非 id时 赋值无法正常展示的问题
2. 【优化】cascader 新增 delChildren 属性配置，用于删除叶子节点的 children字段
3. 【优化】desensitization 脱敏方法的options属性为字符串时 则表示脱敏类型

### 0.0.2
1. 【修复】修复datetime组件赋值显示错误的问题

### 0.0.0
vue2-vant-dict最初版本