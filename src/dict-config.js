//引入请求字典接口
import {getDictCodeApi} from "@/api/module/common-api.js"
//引入本地缓存数据
import localDictCodes from "@/assets/data/dict.js"

export default {
  getDictCodeApi,
  localDictCodes,
  treeSetting: {
    labelField: "areaName"
  },
  format: {
    value: "value",
    label: "label",
    type: "vanType",
    color: "vanColor",
    disabled: "vanDisabled"
  },
  isGetAll: false,
  usuallyGetDictTypes: "SPORTS,FRUITS",
  usuallyGetDictTypesByApi: false
}