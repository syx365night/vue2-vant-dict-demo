export function getDictCodeApi(options) {
  console.log("🚀 ~ file: common-api.js ~ line 3 ~ getDictCodeApi ~ options", options)
  var promise = new Promise(function(resolve) {
    const dictCodeList = {
      PERSON_TAG: [
        {
          "vanType": "success",
          "value": "0",
          "label": "可爱"
        }, 
        {
          "vanType": "info",
          "value": "1",
          "label": "大方"
        },
        {
          "vanType": "warning",
          "value": "2",
          "label": "善良"
        },
        {
          "vanType": "danger",
          "value": "3",
          "label": "上进"
        },
        {
          "vanType": "danger",
          "value": "4",
          "label": "勤劳"
        },
        {
          "vanType": "danger",
          "value": "5",
          "label": "诚实"
        },
        {
          "value": "6",
          "label": "慈祥"
        }
      ],
      COLORS: [
        {
          "value": "0",
          "label": "红"
        }, 
        {
          "value": "1",
          "label": "绿"
        },
        {
          "value": "2",
          "label": "蓝"
        }
      ],
      "SEX": [
        {
          "value": "0",
          "label": "男"
        }, 
        {
          "value": "1",
          "label": "女"
        },
        {
          "value": "2",
          "label": "未知",
          "vanDisabled": true
        }
      ],
      "FRUITS": [
        {
          "value": "1",
          "label": "西瓜",
          "vanDisabled": true,
          "id": "1",
          "parentId": "0"
        },
        {
          "value": "2",
          "label": "草莓",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "3",
          "label": "莲雾",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "4",
          "label": "荔枝",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "5",
          "label": "青梅",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "6",
          "label": "芒果",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "7",
          "label": "火龙果",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "8",
          "label": "山竹",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "9",
          "label": "橙子",
          "id": "2",
          "parentId": "0"
        },
        {
          "value": "10",
          "label": "人参果",
          "id": "2",
          "parentId": "0"
        }
      ],
      "SPORTS": [
        {
          "value": "1",
          "label": "篮球"
        },
        {
          "value": "2",
          "label": "足球"
        },
        {
          "value": "3",
          "label": "乒乓球"
        },
        {
          "value": "4",
          "label": "羽毛球"
        },
        {
          "value": "5",
          "label": "网球"
        },
        {
          "value": "6",
          "label": "保龄球"
        },
        {
          "value": "7",
          "label": "水球"
        },
        {
          "value": "8",
          "label": "台球"
        },
        {
          "value": "9",
          "label": "跑步"
        },
        {
          "value": "10",
          "label": "游泳"
        }
      ],
      "STATUSHIERARCHY": [{
        "value": "5",
        "label": "Lv5认证"
      }, {
        "value": "4",
        "label": "Lv4认证"
      }, {
        "value": "3",
        "label": "Lv3认证"
      }, {
        "value": "2",
        "label": "Lv2认证"
      }, {
        "value": "1",
        "label": "LV1认证"
      }],
      "AAC058": [{
        "value": "01",
        "label": "居民身份证(户口簿)"
      }, {
        "value": "02",
        "label": "中国人民解放军军官证"
      }, {
        "value": "03",
        "label": "中国人民武装警察警官证"
      }, {
        "value": "04",
        "label": "香港特区护照/港澳居民来往内地通行证"
      }, {
        "value": "05",
        "label": "澳门特区护照/港澳居民来往内地通行证"
      }, {
        "value": "06",
        "label": "台湾居民来往大陆通行证"
      }, {
        "value": "07",
        "label": "外国人永久居留证"
      }, {
        "value": "08",
        "label": "外国人护照"
      }, {
        "value": "09",
        "label": "残疾人证"
      }, {
        "value": "10",
        "label": "军烈属证明"
      }, {
        "value": "11",
        "label": "外国人就业证"
      }, {
        "value": "12",
        "label": "外国专家证"
      }, {
        "value": "13",
        "label": "外国人常驻记者证"
      }, {
        "value": "14",
        "label": "台港澳人员就业证"
      }, {
        "value": "15",
        "label": "回国(来华)定居专家证"
      }, {
        "value": "16",
        "label": "港澳台居民居住证"
      }, {
        "value": "90",
        "label": "社会保障卡"
      }, {
        "value": "91",
        "label": "中国护照号码"
      }, {
        "value": "99",
        "label": "其他身份证件"
      }],
      "AAC005": [{
        "value": "01",
        "label": "汉族"
      }, {
        "value": "02",
        "label": "蒙古族"
      }, {
        "value": "20",
        "label": "傈傈族"
      }, {
        "value": "4",
        "label": "停用"
      }, {
        "value": "5",
        "label": "冻结"
      }, {
        "value": "9",
        "label": "注销"
      }],
      "LEGAL_STATUS": [{
        "value": "000",
        "label": "锁定"
      }, {
        "value": "001",
        "label": "正常"
      }],
      "PERSON_TYPE": [{
        "value": "005",
        "label": "民营企业代表"
      }, {
        "value": "004",
        "label": "个体工商户"
      }, {
        "value": "003",
        "label": "机关事业单位法人",
        "vanDisabled": true
      }, {
        "value": "002",
        "label": "社团法人",
        "vanColor": "#7232dd"
      }, {
        "value": "001",
        "label": "企业法人",
        "vanType": "warning"
      }],
      "UNIT_TYPE": [{
        "value": "3",
        "label": "地税编号"
      }, {
        "value": "2",
        "label": "统一信用代码"
      }, {
        "value": "1",
        "label": "组织机构代码"
      }],
      "MCC_TYPE": [{
        "value": "1",
        "label": "农,林,牧,渔业"
      }, {
        "value": "10",
        "label": "金融业"
      }, {
        "value": "11",
        "label": "房地产业"
      }, {
        "value": "12",
        "label": "租赁和商务服务业"
      }, {
        "value": "13",
        "label": "科学研究、技术服务和地质勘查业"
      }, {
        "value": "14",
        "label": "水利、环境和公共设施管理业"
      }],
      "UNIT_STATUS": [{
        "value": "1",
        "label": "注销"
      }, {
        "value": "2",
        "label": "正常"
      }]
    }

    let codeData = {}
    if (options&&options.dictType) {
      const typeList = options.dictType.split(",")
      for (let i = 0; i < typeList.length; i++) {
        const type = typeList[i]
        codeData[type] = dictCodeList[type]
      }
    } else {
      codeData = dictCodeList
    }
    const data = {
      "code": 0,
      "data": {
        dictData: codeData,
        version: "0.0.2"
      },
      message: "成功",
      timestamp: "1594565635",
      type: "info"
    }
    setTimeout(() => { resolve(data.data) }, 1000)
  })
  return promise
}

//获取全局配置  包含版本号
export function getGlobalConfigApi() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({version: "0.0.2"})
    }, 1000)
  })
}