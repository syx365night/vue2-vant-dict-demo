export default [
  {
    path: "/",
    redirect: "/home"
  },
  {
    name: "home",
    path: "/home",
    meta: {
      title: "目录"
    },
    component: () => import(/* webpackChunkName: "commonModule" */ "@/views/home.vue")
  },
  {
    name: "demoForm",
    path: "/demo-form",
    meta: {
      title: "表单示例"
    },
    component: () => import(/* webpackChunkName: "commonModule" */ "@/views/demo-form.vue")
  },
  {
    name: "demoFilter",
    path: "/demo-filter",
    meta: {
      title: "过滤器示例"
    },
    component: () => import(/* webpackChunkName: "commonModule" */ "@/views/demo-filter.vue")
  },
  {
    name: "demoData",
    path: "/demo-data",
    meta: {
      title: "自定义数据"
    },
    component: () => import(/* webpackChunkName: "commonModule" */ "@/views/demo-data.vue")
  },
  {
    name: "demoFun",
    path: "/demo-function",
    meta: {
      title: "方法"
    },
    component: () => import(/* webpackChunkName: "commonModule" */ "@/views/demo-function.vue")
  },
  {
    name: "demoAdmin",
    path: "/demo-admin",
    meta: {
      title: "iframe通信"
    },
    component: () => import(/* webpackChunkName: "commonModule" */ "@/views/demo-admin.vue")
  }
]