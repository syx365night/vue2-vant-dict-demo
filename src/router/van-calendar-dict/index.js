export default [
  {
    name: "vanCalendarDict",
    path: "/van-calendar-dict",
    meta: {
      title: "vanCalendarDict"
    },
    component: () => import(/* webpackChunkName: "vanCalendarDictModule" */ "@/components/van-calendar-dict")
  }
]