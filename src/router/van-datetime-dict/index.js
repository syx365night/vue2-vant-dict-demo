export default [
  {
    name: "vanDatetimeDict",
    path: "/van-datetime-dict",
    meta: {
      title: "vanDatetimeDict"
    },
    component: () => import(/* webpackChunkName: "vanDatetimeDictModule" */ "@/components/van-datetime-dict")
  }
]