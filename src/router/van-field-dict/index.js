export default [
  {
    name: "vanFieldDict",
    path: "/van-field-dict",
    meta: {
      title: "vanFieldDict"
    },
    component: () => import(/* webpackChunkName: "vanFieldModule" */ "@/components/van-field-dict")
  }
]