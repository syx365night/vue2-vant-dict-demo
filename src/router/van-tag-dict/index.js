export default [
  {
    name: "vanTagDict",
    path: "/van-tag-dict",
    meta: {
      title: "vanTagDict"
    },
    component: () => import(/* webpackChunkName: "vanTagDictModule" */ "@/components/van-tag-dict")
  }
]