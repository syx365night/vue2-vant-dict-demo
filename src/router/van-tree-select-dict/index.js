export default [
  {
    name: "vanTreeSelectDict",
    path: "/van-tree-select-dict",
    meta: {
      title: "vanTreeSelectDict"
    },
    component: () => import(/* webpackChunkName: "vanTreeSelectDictModule" */ "@/components/van-tree-select-dict")
  }
]