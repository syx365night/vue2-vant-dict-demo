import Vue from "vue"
import Router from "vue-router"
Vue.use(Router)

let routes = []
const routerContext = require.context("./", true, /index\.js$/)
routerContext.keys().forEach(route => {
  //排除本身文件
  if (route.startsWith("./index")) {
    return
  }
  const routerModule = routerContext(route)
  routes = routes.concat([...(routerModule.default || routerModule)])
})

const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  linkActiveClass: "active",
  routes
})

const router = createRouter()

export default router