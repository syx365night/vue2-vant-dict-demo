export default [
  {
    name: "vanCheckboxDict",
    path: "/van-checkbox-dict",
    meta: {
      title: "vanCheckboxDict"
    },
    component: () => import(/* webpackChunkName: "vanSelectDictModule" */ "@/components/van-checkbox-dict")
  }
]