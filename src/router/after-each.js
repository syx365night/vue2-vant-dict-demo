/*
 * @Description: 路由跳转设置浏览器title
 * @Version: 0.1
 * @Autor: Chenyt
 */
function setTitle(title) {
  document.title = title
  document.head.querySelector("title").innerText = title
}

function afterEachHandler(to, from) {
  let { title } = to.meta
  title || (title = to.name)
  if (title) {
    setTitle(title)
  }
}

export default afterEachHandler

