export default [
  {
    name: "vanTabsDict",
    path: "/van-tabs-dict",
    meta: {
      title: "vanTabsDict"
    },
    component: () => import(/* webpackChunkName: "vanTabsDictModule" */ "@/components/van-tabs-dict")
  }
]