/*
 * @Description: 演示或维护页面切换
 * @Version: 0.1
 * @Autor: Chenyt
 */

function beforeEachHandler(to, from, next) {
  next()
}
export {
  beforeEachHandler
}
