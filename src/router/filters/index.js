export default [
  {
    name: "filters-dict",
    path: "/filters/dict",
    meta: {
      title: "dictFilters"
    },
    component: () => import(/* webpackChunkName: "filtersModule" */ "@/components/filters/dict")
  },
  {
    name: "filters-other",
    path: "/filters/other",
    meta: {
      title: "otherFilters"
    },
    component: () => import(/* webpackChunkName: "filtersModule" */ "@/components/filters/other")
  }
]