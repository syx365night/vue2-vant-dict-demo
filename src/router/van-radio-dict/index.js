export default [
  {
    name: "vanRadioDict",
    path: "/van-radio-dict",
    meta: {
      title: "vanRadioDict"
    },
    component: () => import(/* webpackChunkName: "vanRadioDictModule" */ "@/components/van-radio-dict")
  }
]