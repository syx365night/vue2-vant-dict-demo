export default [
  {
    name: "vanMultipleSelectDict",
    path: "/van-multiple-select-dict",
    meta: {
      title: "vanMultipleSelectDict"
    },
    component: () => import(/* webpackChunkName: "vanMultipleSelectDictModule" */ "@/components/van-multiple-select-dict")
  }
]