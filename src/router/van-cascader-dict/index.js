export default [
  {
    name: "vanCascaderDict",
    path: "/van-cascader-dict",
    meta: {
      title: "vanCascaderDict"
    },
    component: () => import(/* webpackChunkName: "vanSelectDictModule" */ "@/components/van-cascader-dict")
  }
]