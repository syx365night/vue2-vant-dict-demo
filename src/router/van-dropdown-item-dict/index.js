export default [
  {
    name: "vanDropdownItemDict",
    path: "/van-dropdown-item-dict",
    meta: {
      title: "vanDropdownItemDict"
    },
    component: () => import(/* webpackChunkName: "vanDropdownItemModule" */ "@/components/van-dropdown-item-dict")
  }
]