export default [
  {
    name: "vanButtonDict",
    path: "/van-button-dict",
    meta: {
      title: "vanButtonDict"
    },
    component: () => import(/* webpackChunkName: "vanButtonDictModule" */ "@/components/van-button-dict")
  }
]