export default [
  {
    name: "functions-dict",
    path: "/functions/functions-dict",
    meta: {
      title: "functions-dict"
    },
    component: () => import(/* webpackChunkName: "functionsModule" */ "@/components/functions/functions-dict")
  },
  {
    name: "functions-other",
    path: "/functions/functions-other",
    meta: {
      title: "functions-other"
    },
    component: () => import(/* webpackChunkName: "functionsModule" */ "@/components/functions/functions-other")
  }
]