export default [
  {
    name: "vanSelectDict",
    path: "/van-select-dict",
    meta: {
      title: "vanSelectDict"
    },
    component: () => import(/* webpackChunkName: "vanSelectDictModule" */ "@/components/van-select-dict")
  }
]