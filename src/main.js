import Vue from "vue"
import App from "./App.vue"
import Router from "vue-router"
import router from "@/router"
Vue.config.productionTip = false

import "@/styles/index.less"

// 修改路由跳转报错的bug-start
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error)
}
import { beforeEachHandler } from "@/router/before-each"
import afterEachHandler from "@/router/after-each"
router.beforeEach(beforeEachHandler)
router.afterEach(afterEachHandler)

import Vant from "vant-xiaobu"
import "vant-xiaobu/lib/index.css"

Vue.use(Vant)

// 监听子页面想父页面的传参
window.addEventListener("message", function(event) {
  //此处执行事件
  // 通过origin对消息进行过滤，避免遭到XSS攻击
  if (event.origin === "https://www.xiaobusoft.com") {
    if (event.data.action === "globalData") { alert(JSON.stringify(event.data)) }
  }
})

import {getGlobalConfigApi} from "@/api/module/common-api.js"
import dictConfig from "@/dict-config.js"
import vue2VantDict from "vue2-vant-dict"

//获取当前版本号
getGlobalConfigApi().then(data => {
  const {version} = data
  Object.assign(dictConfig, {version})
  Vue.use(vue2VantDict, dictConfig)
  new Vue({
    router,
    render: h => h(App)
  }).$mount("#app")
})
