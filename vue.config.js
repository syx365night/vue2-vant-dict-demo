const path = require("path")
module.exports = {
  publicPath: "./",
  lintOnSave: false,
  pages: {
    index: {
      entry: "src/main.js",
      template: "public/index.html",
      filename: "index.html"
    }
  },
  // 强制内联CSS
  css: { extract: false },
  chainWebpack: config => {
    config.module.rule("js")
      .include
      .add(path.resolve(__dirname, "src")).end()
      .use("babel")
      .loader("babel-loader")
      .tap(options => {
        return options
      })
    config.resolve.alias
      .set("@", path.resolve(__dirname, "src"))
  }
}